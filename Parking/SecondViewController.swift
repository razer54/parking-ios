//
//  SecondViewController.swift
//  Parking
//
//  Created by Pierre Paci on 22/11/2015.
//  Copyright © 2015 Pierre Paci. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class SecondViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var location: MKMapView!
    let locationManager = CLLocationManager();
    var Parkings = Set<MKPointAnnotation>()
    var selectedAnnotation: MKPointAnnotation!
    var annotation = MKPointAnnotation()
    var currentCoord = CLLocationCoordinate2D()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.location.showsTraffic = true
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.distanceFilter = 200
        self.location.showsUserLocation = true
        self.location.mapType = MKMapType.Hybrid
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        
        GetParking()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        self.location.setCenterCoordinate(newLocation.coordinate, animated: true)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error: " +  error.localizedDescription)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetParking() { // récupère les données des parkings
        Alamofire.request(.GET, "https://geoservices.grand-nancy.org/arcgis/rest/services/public/VOIRIE_Parking/MapServer/0/query?where=1%3D1&text&objectIds&time&geometry&geometryType=esriGeometryEnvelope&inSR&spatialRel=esriSpatialRelIntersects&relationParam&outFields=%2A&returnGeometry=true&maxAllowableOffset&geometryPrecision&outSR=4326&returnIdsOnly=false&returnCountOnly=false&orderByFields&groupByFieldsForStatistics&outStatistics&returnZ=false&returnM=false&gdbVersion&returnDistinctValues=false&f=json")
            .responseJSON { response in
                if let JSON = response.result.value {
                    
                    for parking in (JSON["features"]!! as? NSArray)!{
                        let nom = parking["attributes"]!!["NOM"] as! String
                        let places = (parking["attributes"]!!["PLACES"] as? NSNumber)?.stringValue
                        let y = parking["geometry"]!!["y"] as! Double
                        let x = parking["geometry"]!!["x"] as! Double
                        
                        let coord = CLLocationCoordinate2D(latitude: y,longitude: x)
                        
                        if(places != nil){
                            let Annotation = MKPointAnnotation()
                            
                            Annotation.title=nom
                            Annotation.subtitle = (places != "0" ? places : "complet")
                            Annotation.coordinate=coord
                            self.mapView(self.location,viewForAnnotation: Annotation)
                            self.location.addAnnotation(Annotation)
                        }
                        
                    }
                }
            }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? { //initialise les annotation
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }

        var view = mapView.dequeueReusableAnnotationViewWithIdentifier("ann") as? MKPinAnnotationView
        if view == nil {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "ann")
            view?.canShowCallout = true
            view?.pinTintColor = UIColor(red: 0, green: 0, blue: 255, alpha: 1)
            view!.animatesDrop = true
            
            // Add detail button to right callout
            let itineraireButton = UIButton(type: UIButtonType.Custom) as UIButton
            itineraireButton.frame.size.width = 50
            itineraireButton.frame.size.height = 60
            itineraireButton.backgroundColor = UIColor.blueColor()
            itineraireButton.setImage(UIImage(named: "car4"), forState:UIControlState.Normal)
            view!.leftCalloutAccessoryView = itineraireButton
            
        } else {
            view?.annotation = annotation
        }
        return view
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) { // Delegate appelé à l'appui sur le callout
        let url = NSURL(string: "http://maps.apple.com/?daddr=\((view.annotation?.coordinate.latitude)!),\((view.annotation?.coordinate.longitude)!)&t=m") //url pour lancer la naviagation. Correspond à l'adresse du parking
        UIApplication.sharedApplication().openURL(url!)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) { // ????????
        if let destination = segue.destinationViewController as? SecondViewController {
            destination.annotation = selectedAnnotation
        }
    }
}

