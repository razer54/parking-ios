//
//  TodayViewController.swift
//  Parking-local
//
//  Created by Pierre Paci on 09/12/2015.
//  Copyright © 2015 Pierre Paci. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreLocation

class TodayViewController: UIViewController, NCWidgetProviding, CLLocationManagerDelegate {
    
    @IBOutlet var position: UILabel!
    var locationManager = CLLocationManager()
    var location: CLLocation!
    var latitude: String!
    var longitude: String!
    var date: String!
    
    @IBAction func enregistrer(sender: AnyObject) {
        //Localiser la personne
        self.locationManager.requestLocation();
        
        
    }
    @IBAction func localiser(sender: AnyObject) {
        let url = NSURL(string: "http://maps.apple.com/?daddr=\(self.latitude),\(self.longitude)&t=m") //url pour lancer la naviagation. Correspond à l'adresse du parking
       
        self.extensionContext!.openURL(url!, completionHandler: nil)
        
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last!
        let mem = NSUserDefaults.standardUserDefaults()
        self.date = NSDate().description
        position.text = "Garé le : \(self.date)"
        self.latitude = self.location.coordinate.latitude.description
        self.longitude = self.location.coordinate.longitude.description
        mem.setObject(self.date, forKey: "dateToday")
        mem.setObject(self.latitude, forKey: "latitudeToday")
        mem.setObject(self.longitude, forKey: "longitudeToday")
    }
 
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = 200
        let mem = NSUserDefaults.standardUserDefaults()
        self.latitude = mem.stringForKey("latitudeToday")
        self.longitude = mem.stringForKey("longitudeToday")
        self.date = mem.stringForKey("dateToday")
        if(self.date != nil){
            position.text = "Garé le : \(self.date!)"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets
        (defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets) {
            return UIEdgeInsetsZero
    }
    
}
